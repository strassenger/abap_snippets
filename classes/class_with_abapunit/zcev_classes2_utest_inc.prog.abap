*&---------------------------------------------------------------------*
*& Include zcev_classes2_utest_inc
*&---------------------------------------------------------------------*


class ltc_product definition for testing
risk level harmless
duration short.

    private section.
      data: cut type ref to lcl_stock.
      data: result type string.
    methods:
      check_stock_test
        importing
        mv_stock_value type i,
      apply_tests for testing raising cx_static_check,
      setup.

endclass.



class ltc_product implementation.

  method setup.
     "given
     cut = new lcl_stock( ).
  endmethod.

  method check_stock_test.

     "then
     cl_abap_unit_assert=>assert_equals( act = cut->check_stock( mv_stock_value )   "when
                                         exp = 'true' ).

  endmethod.

  method apply_tests.
    check_stock_test( mv_stock_value = 1 ).
    check_stock_test( mv_stock_value = 5 ).
    check_stock_test( mv_stock_value = 1 ).
    check_stock_test( mv_stock_value = 2 ).
  endmethod.


endclass.
