with res as (select
	 ACDOCA.GKONT,
	 ACDOCA.RYEAR,
	 ACDOCA.BELNR,
	 ACDOCA.BLART,
	 ACDOCA.VTWEG,
	 ACDOCA.WERKS,
	 SUM ( CASE WHEN ACDOCA.RACCT IN('0044735000' ,
	 '0044270000',
	 '0044736000' ,
	 '0044230000') 
		and ACDOCA.BLART in('AB',
	 'DA',
	 'DG',
	 'DR',
	 'RV',
	 'YV') 
		THEN ACDOCA.HSL 
		else 0 
		END) AUTRES_TAXE,
	 SUM ( CASE WHEN ACDOCA.RACCT = '0044551000' 
		THEN ACDOCA.HSL 
		else 0 
		END) TVA,
	 SUM (CASE WHEN to_decimal(replace ( ACDOCA.RACCT,
	 'M',
	 '')) between 70001000 
		AND 70999999 
		THEN ACDOCA.HSL 
		else 0 
		END) NET 
	from "SAPABAP1"."ACDOCA" 
	where ACDOCA.RCLNT='100' 
	and ACDOCA.BLART in('AB',
	 'DA',
	 'DG',
	 'DR',
	 'RV',
	 'YV') 
	GROUP BY ACDOCA.GKONT,
	 ACDOCA.RYEAR,
	 ACDOCA.BELNR,
	 ACDOCA.BLART,
	 ACDOCA.VTWEG,
	 ACDOCA.WERKS),
	 sub_query as ( 
	 
SELECT
	 KNA1.NAME1,
	 res.GKONT,
	 res.BELNR,
	 res.BLART,
	 res.VTWEG,
	 res.WERKS,
	 KNA1.stras||' '||KNA1.pstlz||' '||KNA1.ort01||', '||KNA1.land1||'.' AS Adresse,
	 max(case when BUT0ID.TYPE = 'BUP002' 
		then BUT0ID.IDNUMBER 
		end )As CA,
	 max(case when BUT0ID.TYPE = 'ZAI001' 
		then BUT0ID.IDNUMBER 
		end )As AI,
	 max(case when BUT0ID.TYPE = 'ZNIF01' 
		then BUT0ID.IDNUMBER 
		end) As NIF,
	 res.AUTRES_TAXE,
	 res.TVA,
	 res.NET 
	FROM res 
	LEFT OUTER JOIN "SAPABAP1"."KNA1" ON KNA1.KUNNR = res.GKONT 
	LEFT OUTER join "SAPABAP1"."BUT0ID" ON RES.GKONT = BUT0ID.PARTNER 
	where RES.RYEAR = '2017' 
	and (res.AUTRES_TAXE!=0 
		or res.TVA !=0 
		or res.NET !=0) 
	GROUP BY KNA1.NAME1,
	 res.GKONT,
	 res.BELNR,
	 res.BLART,
	 res.VTWEG,
	 res.WERKS,
	 res.AUTRES_TAXE,
	 res.TVA,
	 res.NET,
	 KNA1.stras||' '||KNA1.pstlz||' '||KNA1.ort01||', '||KNA1.land1||'.' ) 
	 
SELECT
	 NAME1,
	 GKONT,
	 BELNR,
	 BLART,
	 VTWEG,
	 WERKS,
	 Adresse,
	 CA,
	 AI,
	 NIF,
	 AUTRES_TAXE,
	 TVA,
	 NET,
	 case when row_number()over()= 1 
then sum(AUTRES_TAXE)over() 
else 0 
end as Total_AUTRE_TAXE,
	 case when row_number()over()= 1 
then sum(TVA)over() 
else 0 
end as Total_TVA,
	 case when row_number()over()= 1 
then sum(NET)over() 
else 0 
end as Total_NET 
from sub_query 