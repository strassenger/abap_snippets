*&---------------------------------------------------------------------*
*& Report      : zcevabap_prices
*&
*& Module      : SD
*& Created by  : RISSOLAH
*& Created on  : 25.4.2019
*& Version     : Ver 1.0
*& Description : Les prix Brandt Store
*& TR          : Local Objects
*&---------------------------------------------------------------------*
report zcevabap_prices.


*&---------------------------------------------------------------------*
*& TABLESm
*&---------------------------------------------------------------------*
tables mara.


*&---------------------------------------------------------------------*
*& DATA
*&---------------------------------------------------------------------*
data o_prices type ref to zcl_prix_bs.


*&---------------------------------------------------------------------*
*& SELECTION SCREEN
*&---------------------------------------------------------------------*
selection-screen begin of block rep01 with frame title text-001.
    select-options: s_arts for mara-matnr.
selection-screen end of block rep01.


*&---------------------------------------------------------------------*
*& AT SELECTION-SCREEN
*&---------------------------------------------------------------------*
at selection-screen.
* Get the articles from selection and send them to the constructor
o_prices = new #( it_articles = s_arts[] ).


*&---------------------------------------------------------------------*
*& START-OF-SELECTION
*&---------------------------------------------------------------------*
start-of-selection.

* Display the articles
o_prices->display_alv(
    it_alvresults =
    o_prices->get_prices(
        exporting it_articles = o_prices->get_articles( )
        )  ).