*----------------------------------------------------------------------*
*       CLASS lcl_alv DEFINITION
*----------------------------------------------------------------------*
CLASS lcl_alv DEFINITION.
  PUBLIC SECTION.

    METHODS select_alv
              IMPORTING amount TYPE i.

    METHODS display_alv.

  PROTECTED SECTION.

    DATA: o_table   TYPE REF TO cl_salv_table,
          o_columns TYPE REF TO cl_salv_columns_table.

    DATA t_alv TYPE STANDARD TABLE OF alv_tab.

    METHODS set_alv_header.

    METHODS set_alv_footer.

ENDCLASS.                    "lcl_alv DEFINITION
Class Implementation



*----------------------------------------------------------------------*
*       CLASS lcl_alv IMPLEMENTATION
*----------------------------------------------------------------------*
CLASS lcl_alv IMPLEMENTATION.

  METHOD select_alv.

    SELECT *
      FROM alv_tab
      INTO CORRESPONDING FIELDS OF TABLE t_alv
          UP TO amount ROWS.

  ENDMETHOD.                    "SELECT_ALV

  METHOD display_alv.

    TRY.
        cl_salv_table=>factory(
          IMPORTING
            r_salv_table = o_table
          CHANGING
            t_table      = t_alv ).
      CATCH cx_salv_msg.
    ENDTRY.

    o_columns = o_table->get_columns( ).
    o_columns->set_optimize( abap_true ).

*   Set the alv Header
    me->set_alv_header( ).
*   Set the alv Footer
    me->set_alv_footer( ).

    o_table->display( ).

  ENDMETHOD.                    "display_alv

  METHOD set_alv_header.

    DATA: column TYPE i,
          row    TYPE i.

    DATA: column_char TYPE c,
          row_char    TYPE c.

    DATA: text    TYPE string,
          tooltip TYPE string.

    DATA icon TYPE icon_d.

    DATA: o_header_element TYPE REF TO cl_salv_form_layout_grid,
          o_grid        TYPE REF TO cl_salv_form_layout_grid,

          o_header_info TYPE REF TO cl_salv_form_header_info,
          o_action_info TYPE REF TO cl_salv_form_action_info,

          o_text TYPE REF TO cl_salv_form_text,

          t_data_grid TYPE STANDARD TABLE OF
                           REF TO cl_salv_form_layout_data_grid,
          o_data_grid TYPE REF TO cl_salv_form_layout_data_grid,

          o_icon TYPE REF TO cl_salv_form_icon.

*   Create an instance of Header objects
    CREATE OBJECT o_header_element
      EXPORTING
        columns = 2.

*   Create a Text with layout HEADER INFORMATION
    o_header_info = o_header_element->create_header_information(
      row = 1
      column = 1
      text     = 'Text of Header Info'
      tooltip  = 'Tooltip of Header Info' ).

*   Create a Text with layout ACTION INFORMATION
    o_action_info = o_header_element->create_action_information(
      row = 2
      column = 1
      text     = 'Text of Action Info'
      tooltip  = 'Tooltip of Action Info' ).                "#EC NOTEXT

*   Create a Grid with 4 rows and 3 columns
    o_grid = o_header_element->create_grid( row = 4 column = 3 ).

    DO 4 TIMES.

      ADD 1 TO row.

      DO 3 TIMES.

        ADD 1 TO column.

        MOVE column TO column_char.
        MOVE row    TO row_char.

        CONCATENATE 'Text of' column_char 'column in line' row_char
                                      INTO text SEPARATED BY space.

        CONCATENATE 'Tooltip of' column_char 'column in line' row_char
                                      INTO tooltip SEPARATED BY space.

*       Insert a text into on the <column> of <row> line
        o_text = o_grid->create_text(
            row     = row
            column  = column
            text    = text
            tooltip = tooltip ).

*       Get the layout of grid
        o_data_grid ?= o_text->get_layout_data( ).

        APPEND o_data_grid TO t_data_grid.
        CLEAR o_text.

      ENDDO.

      CASE row.
        WHEN 1.
          MOVE '<at:var at:name="19" />' TO icon.
          tooltip = 'Information Message'.
        WHEN 2.
          MOVE '<at:var at:name="1A" />' TO icon.
          tooltip = 'Warning Message'.
        WHEN 3.
          MOVE '<at:var at:name="1B" />' TO icon.
          tooltip = 'Error Message'.
        WHEN 4.
          MOVE '<at:var at:name="1C" />' TO icon.
          tooltip = 'Question Message'.
      ENDCASE.

*     Create an instance of Icon Objects
      CREATE OBJECT o_icon
        EXPORTING
          icon    = icon
          tooltip = tooltip.

*     Set Icon on Grid
      CALL METHOD o_grid->set_element
        EXPORTING
          row       = row
          column    = 4
          r_element = o_icon.

      CLEAR column.
    ENDDO.

*   Set Header to Top of ALV
    o_table->set_top_of_list( o_header_element ).

  ENDMETHOD.                    "set_alv_header

  METHOD set_alv_footer.

    DATA: column TYPE i,
          row    TYPE i.

    DATA: column_char TYPE c,
          row_char    TYPE c.

    DATA: text    TYPE string,
          tooltip TYPE string.

    DATA icon TYPE icon_d.

    DATA: o_footer_element TYPE REF TO cl_salv_form_layout_grid,
          o_grid           TYPE REF TO cl_salv_form_layout_grid,

          o_header_info TYPE REF TO cl_salv_form_header_info,
          o_action_info TYPE REF TO cl_salv_form_action_info,

          o_text TYPE REF TO cl_salv_form_text,

          t_data_grid TYPE STANDARD TABLE OF
                           REF TO cl_salv_form_layout_data_grid,
          o_data_grid TYPE REF TO cl_salv_form_layout_data_grid,

          o_icon TYPE REF TO cl_salv_form_icon.

*   Create an instance of Footer objects
    CREATE OBJECT o_footer_element
      EXPORTING
        columns = 2.

*   Create a Text with layout HEADER INFORMATION
    o_header_info = o_footer_element->create_header_information(
      row = 1
      column = 1
      text     = 'Text of Footer Info'
      tooltip  = 'Tooltip of Footer Info' ).

*   Create a Text with layout ACTION INFORMATION
    o_action_info = o_footer_element->create_action_information(
      row = 2
      column = 1
      text     = 'Text of Action Info'
      tooltip  = 'Tooltip of Action Info' ).                "#EC NOTEXT

*   Create a Grid with 3 rows and 3 columns
    o_grid = o_footer_element->create_grid( row = 4 column = 3 ).

    DO 4 TIMES.

      ADD 1 TO row.

      DO 2 TIMES.

        ADD 1 TO column.

        MOVE column TO column_char.
        MOVE row    TO row_char.

        CONCATENATE 'Text of' column_char 'column in line' row_char
                                      INTO text SEPARATED BY space.

        CONCATENATE 'Tooltip of' column_char 'column in line' row_char
                                      INTO tooltip SEPARATED BY space.

*       Insert a text into on the <column> of <row> line
        o_text = o_grid->create_text(
            row     = row
            column  = column
            text    = text
            tooltip = tooltip ).

*       Get the layout of grid
        o_data_grid ?= o_text->get_layout_data( ).

        APPEND o_data_grid TO t_data_grid.
        CLEAR o_text.

      ENDDO.

      CASE row.
        WHEN 1.
          MOVE '<at:var at:name="19" />' TO icon.
          tooltip = 'Information Message'.
        WHEN 2.
          MOVE '<at:var at:name="1A" />' TO icon.
          tooltip = 'Warning Message'.
        WHEN 3.
          MOVE '<at:var at:name="1B" />' TO icon.
          tooltip = 'Error Message'.
        WHEN 4.
          MOVE '<at:var at:name="1C" />' TO icon.
          tooltip = 'Question Message'.
      ENDCASE.

*     Set Icon on Grid
      CREATE OBJECT o_icon
        EXPORTING
          icon    = icon
          tooltip = tooltip.

*     Create an instance of Icon Objects
      CALL METHOD o_grid->set_element
        EXPORTING
          row       = row
          column    = 3
          r_element = o_icon.

      CLEAR column.
    ENDDO.

*   Set Header to Top of ALV
    o_table->set_end_of_list( o_footer_element ).

  ENDMETHOD.                    "set_alv_footer


ENDCLASS.                    "lcl_alv IMPLEMENTATION



******************************************************************



DATA o_alv TYPE REF TO lcl_alv.


START-OF-SELECTION.

CREATE OBJECT o_alv.

o_alv->select_alv( 30 ).
o_alv->display_alv( ).