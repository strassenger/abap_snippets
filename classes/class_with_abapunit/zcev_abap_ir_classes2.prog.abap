*&---------------------------------------------------------------------*
*& Report      : zcev_abap_ir_classes2
*&
*& Module      : MM
*& Created by  : RISSOLAH
*& Created on  : 30.03.2019
*& Version     : 1.0
*& Description : Classe test 2
*& TR          :
*&---------------------------------------------------------------------*

report zcev_abap_ir_classes2.




*&---------------------------------------------------------------------*
*&                      INCLUDES
*&---------------------------------------------------------------------*

include zcev_abap_ir_classes2_inc.
include zcev_classes2_utest_inc.




*&---------------------------------------------------------------------*
*&                      DATA
*&---------------------------------------------------------------------*

data : go_prd_tv        type ref to lcl_product,
       go_prd_washmach  like go_prd_tv,
       go_mag_alger     type ref to lcl_magazin,
       go_stk_tvalger   type ref to lcl_stock,
       go_stk_wmalger   like go_stk_tvalger.


constants : gc_stock_value_test type int4 value 5.


start-of-selection.

create object go_prd_tv.
create object go_prd_washmach.
create object go_mag_alger.
create object go_stk_tvalger.
create object go_stk_wmalger.


*data(go_prd_tv2) = new lcl_product( ).



*&---------------------------------------------------------------------*
*&                      INSERT
*&---------------------------------------------------------------------*


go_prd_tv->set_product_attributes(
  exporting
    iv_product_number      = 'MAD10032'
    iv_product_designation = 'Téléviseur 32 pouce'
).

go_prd_washmach->set_product_attributes(
  exporting
    iv_product_number      = 'WA100LLTOP'
    iv_product_designation = 'Machine a laver TOP'
).


go_mag_alger->set_magazin_attributes(
  exporting
    iv_magazin_number      = '1006'
    iv_magazin_designation = 'Magazin matière première alger'
).


go_stk_tvalger->set_stock_attributes(
  exporting
    iv_product_number = 'MAD10032'
    iv_magazin_number = '1006'
    iv_stock_quantity = 20
).

go_stk_wmalger->set_stock_attributes(
  exporting
    iv_product_number = 'WA100LLTOP'
    iv_magazin_number = '1006'
    iv_stock_quantity = 50
).




*&---------------------------------------------------------------------*
*&                      DISPLAY
*&---------------------------------------------------------------------*

write : 'Stock de : '(stk).
go_prd_tv->display_product_attributes( ).
go_stk_tvalger->display_stock_attributes( ).

new-line.
write '**********************'.
new-line.

write : 'Stock de : '(stk).
go_prd_washmach->display_product_attributes( ).
go_stk_wmalger->display_stock_attributes( ).


new-line.
write : 'Test stock sur ', gc_stock_value_test.
new-line.
write '**********************'.
new-line.
write : 'Etat stock : ', go_stk_tvalger->check_stock( gc_stock_value_test ).





*&---------------------------------------------------------------------*
*&                      ALV
*&---------------------------------------------------------------------*
