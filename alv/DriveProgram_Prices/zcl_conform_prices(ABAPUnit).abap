class zcl_conform_prices definition for testing
risk level harmless
duration long.

  public section.

  protected section.
     types : begin of ty_prixbs,
                matnr type  mara-matnr,
                kbetr type  konp-kbetr,
             end of ty_prixbs,
             begin of ty_articels,
                matnr type  mara-matnr,
             end of ty_articels.

     types:  tty_matnr      type range of matnr,
             tty_prixbs     type standard table of ty_prixbs with default key.
      data:  gt_articles    type tty_matnr,
             gt_result      type tty_prixbs,
             gt_articles_t  type standard table of ty_articels with default key.
      data:  wa_articles_t  like line of gt_articles_t,
             wa_articles    like line of gt_articles,
             wa_result      like line of gt_result.

  private section.
  data: cut type ref to zcl_prices.
  methods :
        article_actions for testing,
        setup.
endclass.



class zcl_conform_prices implementation.

  method setup.

     data wa_articles_t2 like wa_articles_t.

     me->wa_articles_t-matnr = '000000000000000034'.
     append me->wa_articles_t to me->gt_articles_t.



     read table me->gt_articles_t index 1 into wa_articles_t2.


     wa_articles-sign       = 'I'.
     wa_articles-option     = 'EQ'.
     wa_articles-low        = wa_articles_t2-matnr.
     append wa_articles to gt_articles.


     wa_result-matnr = '000000000000000034'.
     wa_result-kbetr = '35000'.

     append wa_result to gt_result.


     clear wa_articles.
     clear wa_articles_t.
     clear wa_articles_t2.
     clear wa_result.


    cut = new #( it_articles = gt_articles  ).
  endmethod.

  method article_actions.


    data gt_result_exp like gt_result.
    data gt_result_act like gt_result.


   gt_result_exp = me->gt_result.
   gt_result_act = cut->get_prices( it_articles = me->gt_articles ).

    cl_abap_unit_assert=>assert_equals(
        msg = 'Get Prices methode'
        exp = gt_result_exp
        act = gt_result_act
        ).

  endmethod.

endclass.