class zcl_cev_hlpr_common definition
  public
  final
  create public .

public section.

  interfaces zif_cev_hlpr_common.
  aliases :
  get_country_name for zif_cev_hlpr_common~get_country_full_name,
  get_all_text     for zif_cev_hlpr_common~get_fields_text.

protected section.
private section.
endclass.



class zcl_cev_hlpr_common implementation.


  method get_country_name.

    select single landx
      from  t005t
      into  @full_country_name
      where spras = @language
      and   land1 = @country_code.

  endmethod.


  method get_all_text.

      data: li_line type table of tline,
            lw_line like line of  li_line,
            in_name type          thead-tdname,
            in_id   type          thead-tdid,
            in_lang type          thead-tdspras,
            in_obj  type          thead-tdobject.

      move: name to in_name,
            id   to in_id,
            lang to in_lang,
            obj  to in_obj.

      call function 'READ_TEXT'
      exporting
         id       = in_id
         language = in_lang
         name     = in_name
         object   = in_obj
      tables
          lines   = li_line
      exceptions
          id                        = 1
          language                  = 2
          name                      = 3
          not_found                 = 4
          object                    = 5
          reference_check           = 6
          wrong_access_to_archive   = 7
          others                    = 8.

       if sy-subrc eq 0.
         loop at li_line into lw_line.
           concatenate lw_line-tdline text
           into text separated by space.
         endloop.
       endif.

  endmethod.

endclass.