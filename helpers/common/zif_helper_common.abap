interface zif_cev_hlpr_common
  public .

  class-methods :
  get_country_full_name
    importing
      value(language) type land1
      value(country_code) type land1
    returning
      value(full_country_name) type landx,

   get_fields_text
    importing
      value(id) type tdid
      value(name) type string
      value(lang) type land1
      value(obj) type tdobject
    returning
      value(text) type string.

endinterface.
