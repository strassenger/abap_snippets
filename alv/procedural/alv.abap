*&---------------------------------------------------------------------*
*& Report ZZRIS_PRG01
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT ZZRIS_PRG_ALV.

TYPE-POOLS slis.

DATA s_fieldcat type SLIS_FIELDCAT_ALV.
DATA t_fieldcats type TABLE OF SLIS_FIELDCAT_ALV.
DATA s_event type SLIS_ALV_EVENT.
DATA t_events type TABLE OF SLIS_ALV_EVENT.
DATA s_commentaire type SLIS_LISTHEADER.
DATA t_commentaires type TABLE OF SLIS_LISTHEADER.


DATA: gt_adresses type TABLE OF ZZRIS_ADRESS ,
      gs_adress type ZZRIS_ADRESS.

selection-screen  begin of block  b1 with frame  title  text-001.
*SELECTION-SCREEN begin of line.
    PARAMETERS: p_aid type ZADRESS_ID,
                p_ville type ZZRISVILLE,
                p_rue type ZZRISRUE,
                p_code type ZZRISCODE_POSTAL.
    SELECT-OPTIONS s_pays for gs_adress-PAYS.
selection-screen end of  block  b1.
*SELECTION-SCREEN end of line.

* Select data
PERFORM F_SELECT_DATA.
PERFORM DISPLAY_ALV.



FORM DISPLAY_ALV.
          if sy-SUBRC = 0.

          " Events
          CLEAR S_EVENT.
          S_EVENT-NAME = 'TOP_OF_PAGE'.
          S_EVENT-FORM = 'F_TOP_OF_PAGE'.
          APPEND S_EVENT to T_EVENTS.

          CLEAR S_EVENT.
          S_EVENT-NAME = 'END_OF_LIST'.
          S_EVENT-FORM = 'F_END_OF_LIST'.
          APPEND S_EVENT to T_EVENTS.

          CLEAR S_EVENT.
          S_EVENT-NAME = 'USER_COMMAND'.
          S_EVENT-FORM = 'F_USER_COMMAND'.
          APPEND S_EVENT to T_EVENTS.

          " Fields
          CLEAR S_FIELDCAT.
          S_FIELDCAT-FIELDNAME = 'ADRESS_ID'.
          S_FIELDCAT-SELTEXT_L = 'ID'.
          APPEND S_FIELDCAT to T_FIELDCATS.

          CLEAR S_FIELDCAT.
          S_FIELDCAT-FIELDNAME = 'PAYS'.
          S_FIELDCAT-SELTEXT_L = 'Pays'.
          APPEND S_FIELDCAT to T_FIELDCATS.

          CLEAR S_FIELDCAT.
          S_FIELDCAT-FIELDNAME = 'VILLE'.
          S_FIELDCAT-SELTEXT_L = 'Ville'.
          APPEND S_FIELDCAT to T_FIELDCATS.

          CLEAR S_FIELDCAT.
          S_FIELDCAT-FIELDNAME = 'RUE'.
          S_FIELDCAT-SELTEXT_L = 'Rue'.
          APPEND S_FIELDCAT to T_FIELDCATS.

          CLEAR S_FIELDCAT.
          S_FIELDCAT-FIELDNAME = 'CODE_POSTAL'.
          S_FIELDCAT-SELTEXT_L = 'Code Postal'.
          APPEND S_FIELDCAT to T_FIELDCATS.

          CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
           EXPORTING
             I_CALLBACK_PROGRAM                = sy-repid
             IT_FIELDCAT                       = T_FIELDCATS
             IT_EVENTS                         = T_EVENTS
             I_DEFAULT                         = 'X'
             I_SAVE                            = 'A'
            TABLES
              T_OUTTAB                         = GT_ADRESSES
           EXCEPTIONS
             PROGRAM_ERROR                     = 1
             OTHERS                            = 2.
          IF SY-SUBRC <> 0.
            MESSAGE ID sy-MSGID type sy-MSGTY number sy-MSGNO
            with sy-MSGV1 sy-MSGV2 sy-MSGV3 sy-MSGV4.
          ENDIF.


ELSE.
  message 'Wrong inputs !' type 'E'.
ENDIF.
ENDFORM.

FORM F_SELECT_DATA.
  SELECT * from ZZRIS_ADRESS  into TABLE gt_adresses
  where PAYS in s_pays.
ENDFORM.

FORM F_TOP_OF_PAGE.

      REFRESH T_COMMENTAIRES.
      S_COMMENTAIRE-TYP = 'S'.
      S_COMMENTAIRE-INFO = 'Gestion des Adresses'.
      APPEND S_COMMENTAIRE to T_COMMENTAIRES.

      CALL FUNCTION 'REUSE_ALV_COMMENTARY_WRITE'
        EXPORTING
          IT_LIST_COMMENTARY       = T_COMMENTAIRES.
ENDFORM.

FORM F_END_OF_LIST.
  DATA vl_nbr_lignes type N length 4.
  DESCRIBE TABLE GT_ADRESSES LINES vl_nbr_lignes.

      REFRESH T_COMMENTAIRES.
      S_COMMENTAIRE-TYP = 'A'.
      CONCATENATE 'Nombre total des resultat :' vl_nbr_lignes
        into S_COMMENTAIRE-INFO separated by SPACE.
      APPEND S_COMMENTAIRE to T_COMMENTAIRES.

      CALL FUNCTION 'REUSE_ALV_COMMENTARY_WRITE'
        EXPORTING
          IT_LIST_COMMENTARY       = T_COMMENTAIRES
          I_END_OF_LIST_GRID       ='X'.

ENDFORM.