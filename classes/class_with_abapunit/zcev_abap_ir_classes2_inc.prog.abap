*&---------------------------------------------------------------------*
*& Include zcev_abap_ir_classes2_inc
*&---------------------------------------------------------------------*



*&---------------------------------------------------------------------*
*&                      PRODUCTS
*&---------------------------------------------------------------------*

class lcl_product definition.
    public section.
      methods:
          set_product_attributes
              importing
              iv_product_number       type matnr
              iv_product_designation  type string,
          get_product_attributes
              exporting
              ev_product_number       type matnr
              ev_product_designation  type string,
          display_product_attributes.

     protected section.

     private section.
        data :  mv_product_number       type matnr,
                mv_product_designation  type string.
endclass.



class lcl_product implementation.
      method set_product_attributes.
              me->mv_product_number         = iv_product_number.
              me->mv_product_designation    = iv_product_designation.
      endmethod.

      method  get_product_attributes.
              ev_product_number         = me->mv_product_number.
              ev_product_designation    = me->mv_product_designation.
      endmethod.
      method display_product_attributes.
             write : / me->mv_product_number,
                       me->mv_product_designation.
      endmethod.
endclass.




*&---------------------------------------------------------------------*
*&                      MAGAZIN
*&---------------------------------------------------------------------*

class lcl_magazin definition.
    public section.
      methods:
          set_magazin_attributes
              importing
              iv_magazin_number       type lgobe
              iv_magazin_designation  type string,
          get_magazin_attributes
              exporting
              ev_magazin_number       type lgobe
              ev_magazin_designation  type string,
          display_magazin_attributes.

protected section.

     private section.
        data :  mv_magazin_number       type lgobe,
                mv_magazin_designation  type string.
endclass.



class lcl_magazin implementation.
      method set_magazin_attributes.
              me->mv_magazin_number         = iv_magazin_number.
              me->mv_magazin_designation    = iv_magazin_designation.
      endmethod.

      method  get_magazin_attributes.
              ev_magazin_number         = me->mv_magazin_number.
              ev_magazin_designation    = me->mv_magazin_designation.
      endmethod.
      method display_magazin_attributes.
             write : /  me->mv_magazin_number,
                        me->mv_magazin_designation.
      endmethod.
endclass.



*&---------------------------------------------------------------------*
*&                      STOCK
*&---------------------------------------------------------------------*

class lcl_stock definition.
    public section.
      methods:
          set_stock_attributes
              importing
              iv_product_number       type matnr
              iv_magazin_number       type lgobe
              iv_stock_quantity       type int4,
          get_stock_attributes
              exporting
              ev_product_number       type matnr
              ev_magazin_number       type lgobe
              ev_stock_quantity       type int4,
          display_stock_attributes,
          check_stock
              importing
              iv_stock_quantity       type int4
              returning value(rv_is_stock) type string.

protected section.

     private section.
        data :  mv_product_number       type matnr,
                mv_magazin_number       type lgobe,
                mv_stock_quantity       type int4.
endclass.



class lcl_stock implementation.
      method set_stock_attributes.
              me->mv_product_number = iv_product_number.
              me->mv_magazin_number = iv_magazin_number.
              me->mv_stock_quantity = iv_stock_quantity.

      endmethod.

      method  get_stock_attributes.
              ev_product_number     = me->mv_product_number.
              ev_magazin_number     = me->mv_magazin_number.
              ev_stock_quantity     = me->mv_stock_quantity.

      endmethod.
      method display_stock_attributes.
             write : /  'Magazin : ',
                        me->mv_magazin_number,
                        me->mv_stock_quantity.
      endmethod.


  method check_stock.
        rv_is_stock = cond #( when iv_stock_quantity <= 0
                              then 'False'
                              else 'true' ).
  endmethod.


endclass.
