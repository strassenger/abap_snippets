*&---------------------------------------------------------------------*
*& Report ZCEV_SQLMARD
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT ZCEV_SQLMARD.

TYPE-POOLS : SLIS.

TABLES : MARD.

TYPES : BEGIN OF ST_MARD,
        MANDT TYPE MARD-MANDT,
        MATNR TYPE MARD-MATNR,
        LABST TYPE MARD-LABST,
        END OF ST_MARD.

DATA : LT_MARD TYPE TABLE OF ST_MARD,
       IT_FCAT type SLIS_T_FIELDCAT_ALV,
       WA_FCAT type SLIS_FIELDCAT_ALV.

SELECT
  MANDT
  MATNR
  LABST
FROM MARD INTO TABLE LT_MARD
WHERE MATNR ='BAMESA1'.

WA_FCAT-FIELDNAME  = 'MANDT'.
WA_FCAT-SELTEXT_M = 'MANDT'.
APPEND WA_FCAT to IT_FCAT.
ClEAR WA_FCAT.

WA_FCAT-FIELDNAME  = 'MATNR'.
WA_FCAT-SELTEXT_M = 'MATNR'.
APPEND WA_FCAT to IT_FCAT.
ClEAR WA_FCAT.

WA_FCAT-FIELDNAME  = 'LABST'.
WA_FCAT-SELTEXT_M = 'LABST'.
APPEND WA_FCAT to IT_FCAT.
ClEAR WA_FCAT.

CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
 EXPORTING
*   I_INTERFACE_CHECK                 = ' '
*   I_BYPASSING_BUFFER                = ' '
*   I_BUFFER_ACTIVE                   = ' '
*   I_CALLBACK_PROGRAM                = ' '
*   I_CALLBACK_PF_STATUS_SET          = ' '
*   I_CALLBACK_USER_COMMAND           = ' '
*   I_CALLBACK_TOP_OF_PAGE            = ' '
*   I_CALLBACK_HTML_TOP_OF_PAGE       = ' '
*   I_CALLBACK_HTML_END_OF_LIST       = ' '
*   I_STRUCTURE_NAME                  =
*   I_BACKGROUND_ID                   = ' '
*   I_GRID_TITLE                      =
*   I_GRID_SETTINGS                   =
*   IS_LAYOUT                         =
    IT_FIELDCAT                       = IT_FCAT
*   IT_EXCLUDING                      =
*   IT_SPECIAL_GROUPS                 =
*   IT_SORT                           =
*   IT_FILTER                         =
*   IS_SEL_HIDE                       =
*   I_DEFAULT                         = 'X'
*   I_SAVE                            = ' '
*   IS_VARIANT                        =
*   IT_EVENTS                         =
*   IT_EVENT_EXIT                     =
*   IS_PRINT                          =
*   IS_REPREP_ID                      =
*   I_SCREEN_START_COLUMN             = 0
*   I_SCREEN_START_LINE               = 0
*   I_SCREEN_END_COLUMN               = 0
*   I_SCREEN_END_LINE                 = 0
*   I_HTML_HEIGHT_TOP                 = 0
*   I_HTML_HEIGHT_END                 = 0
*   IT_ALV_GRAPHICS                   =
*   IT_HYPERLINK                      =
*   IT_ADD_FIELDCAT                   =
*   IT_EXCEPT_QINFO                   =
*   IR_SALV_FULLSCREEN_ADAPTER        =
* IMPORTING
*   E_EXIT_CAUSED_BY_CALLER           =
*   ES_EXIT_CAUSED_BY_USER            =
  TABLES
    T_OUTTAB                          = LT_MARD
* EXCEPTIONS
*   PROGRAM_ERROR                     = 1
*   OTHERS                            = 2
          .
IF SY-SUBRC <> 0.
* Implement suitable error handling here
ENDIF.