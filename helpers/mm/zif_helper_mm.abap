interface zif_cev_hlpr_mm
  public .

class-methods :
get_article_origin_land
  importing
    value(article) type matnr
    value(division) type werks
  returning
    value(full_land_name) type string.


endinterface.