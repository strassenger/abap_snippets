*&---------------------------------------------------------------------*
*& report zcevtest_alv
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*

report zcevtest_alv.

*type-pools slis.

types:
begin of ty_s_adresse,
  mandt      type adrc-client,
  addrnumber type adrc-addrnumber,
  city       type adrc-city1,
  street     type adrc-street,
  post_code  type adrc-post_code1,
  pays       type adrc-country,
end of ty_s_adresse.


data s_fieldcat type slis_fieldcat_alv.
data t_fieldcats type table of slis_fieldcat_alv.
data s_event type slis_alv_event.
data t_events type table of slis_alv_event.
data s_commentaire type slis_listheader.
data t_commentaires type table of slis_listheader.


data: gt_adresses type table of ty_s_adresse ,
      gs_adresse type ty_s_adresse.


selection-screen  begin of block  b1 with frame  title  text-006.
*selection-screen begin of line.

    select-options:
      s_aid   for gs_adresse-addrnumber,
      s_pays  for gs_adresse-pays,
      s_ville for gs_adresse-city,
      s_rue   for gs_adresse-street,
      s_code  for gs_adresse-post_code.

selection-screen end of  block  b1.
*selection-screen end of line.

* select data

perform f_select_data.
perform display_alv.



form display_alv.
          if sy-subrc = 0.

          " events
          clear s_event.
          s_event-name = 'top_of_page'.
          s_event-form = 'f_top_of_page'.
          append s_event to t_events.

          clear s_event.
          s_event-name = 'end_of_list'.
          s_event-form = 'f_end_of_list'.
          append s_event to t_events.

          clear s_event.
          s_event-name = 'user_command'.
          s_event-form = 'f_user_command'.
          append s_event to t_events.

          " fields
          clear s_fieldcat.
          s_fieldcat-fieldname = 'mandt'.
          s_fieldcat-seltext_l = 'mandant'.
          append s_fieldcat to t_fieldcats.

          clear s_fieldcat.
          s_fieldcat-fieldname = 'addrnumber'.
          s_fieldcat-seltext_l = 'id adresse'.
          append s_fieldcat to t_fieldcats.

          clear s_fieldcat.
          s_fieldcat-fieldname = 'city'.
          s_fieldcat-seltext_l = 'ville'.
          append s_fieldcat to t_fieldcats.

          clear s_fieldcat.
          s_fieldcat-fieldname = 'street'.
          s_fieldcat-seltext_l = 'rue'.
          append s_fieldcat to t_fieldcats.

          clear s_fieldcat.
          s_fieldcat-fieldname = 'post_code'.
          s_fieldcat-seltext_l = 'code postal'.
          append s_fieldcat to t_fieldcats.

          clear s_fieldcat.
          s_fieldcat-fieldname = 'pays'.
          s_fieldcat-seltext_l = 'pays'.
          append s_fieldcat to t_fieldcats.

          call function 'REUSE_ALV_GRID_DISPLAY'
           exporting
             i_callback_program                = sy-repid
             it_fieldcat                       = t_fieldcats
             it_events                         = t_events
             i_default                         = 'x'
             i_save                            = 'a'
            tables
              t_outtab                         = gt_adresses
           exceptions
             program_error                     = 1
             others                            = 2.
          if sy-subrc <> 0.
            message id sy-msgid type sy-msgty number sy-msgno
            with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
          endif.


else.
  message 'wrong inputs !' type 'E'.
endif.
endform.




form f_select_data.

  select * from zcev_t_adress into table @gt_adresses
  where pays     in  @s_pays
  and ADDRNUMBER in  @s_aid
  and city       in  @s_ville
  and post_code  in  @s_code
  and street     in  @s_rue.

endform.




form f_top_of_page.

      refresh t_commentaires.
      s_commentaire-typ = 's'.
      s_commentaire-info = 'gestion des adresses'.
      append s_commentaire to t_commentaires.

      call function 'reuse_alv_commentary_write'
        exporting
          it_list_commentary       = t_commentaires.
endform.

form f_end_of_list.

  data vl_nbr_lignes type n length 4.
  describe table gt_adresses lines vl_nbr_lignes.

      refresh t_commentaires.
      s_commentaire-typ = 'a'.
      concatenate 'nombre total des resultat :' vl_nbr_lignes
        into s_commentaire-info separated by space.
      append s_commentaire to t_commentaires.

      call function 'reuse_alv_commentary_write'
        exporting
          it_list_commentary       = t_commentaires
          i_end_of_list_grid       ='x'.

endform.









*Report End