report zcevabap_cl_salv_table.

*&---------------------------------------------------------------------*
*& Report      : zcevabap_cl_salv_table
*&
*& Module      : ABAP
*& Created by  : RISSOLAH
*& Created on  : 01.08.2018
*& Version     : 1.0
*& Description : Using ALV with Class
*& TR          : None
*&---------------------------------------------------------------------*


data: t_output  type standard table of ZCEV_T_ADRESS,
      wa_output type                   ZCEV_T_ADRESS.




select * from ZCEV_T_ADRESS into table @t_output.

data: o_salv type ref to cl_salv_table.

        cl_salv_table=>factory(
          importing
            r_salv_table   = o_salv
          changing
            t_table        = t_output ).

        o_salv->display( ).