*&---------------------------------------------------------------------*
*& Report ZZBNA_SAPTECH_SCREEN
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT ZZCEV_SCREEN_ALV.

* Tables
TABLES : mara.

* Déclarations
DATA:
*  Material Data
  BEGIN OF wa_mara,
    matnr    TYPE   mara-matnr,         " Material No.
    ERSDA    type   mara-ERSDA,         " Date de création
    mtart    TYPE   mara-mtart,         " Material Type
    bismt    TYPE   mara-bismt,         " Old material No.
    matkl    TYPE   mara-matkl,         " Material group
    meins    TYPE   mara-meins,         " Base Unit of Measure
    brgew    TYPE   mara-brgew,         " Gross Weight
    ntgew    TYPE   mara-ntgew,         " Net Weight
    gewei    TYPE   mara-gewei,         " Weight Unit
  END OF wa_mara,

* Field Catalog
wa_fieldcat  TYPE lvc_s_fcat.

DATA:
* For Material Data
  t_mara LIKE STANDARD TABLE OF wa_mara,
* For Field Catalog
  t_fieldcat TYPE lvc_t_fcat.

DATA:
* User Command
  ok_code TYPE sy-ucomm,
* Reference Variable for Docking Container
  r_dock_container type ref to CL_GUI_DOCKING_CONTAINER,
* Reference Variable for alv grid
  r_grid type REF TO CL_GUI_ALV_GRID.

START-OF-SELECTION.
* To Display the Data
  PERFORM display_output.

FORM display_output.
* To fill the Field Catalog
  PERFORM fill_fieldcat USING :
      'MATNR'    'T_MARA'         'Material No.',
      'ERSDA'    'T_MARA'         'Date of creation',
      'MTART'    'T_MARA'         'Material Type',
      'BISMT'    'T_MARA'         'Old Material No.',
      'MATKL'    'T_MARA'         'Material Group',
      'MEINS'    'T_MARA'         'Base Unit of Measure',
      'BRGEW'    'T_MARA'         'Gross Weight',
      'NTGEW'    'T_MARA'         'Net Weight',
      'GEWEI'    'T_MARA'         'Weight Unit'.
  CALL SCREEN 500.
ENDFORM.                               " Display_output

FORM fill_fieldcat  USING   pv_field   TYPE any
                            pv_tabname TYPE any
                            pv_coltext TYPE any .

  wa_fieldcat-fieldname  = pv_field.
  wa_fieldcat-tabname    = pv_tabname.
  wa_fieldcat-coltext    = pv_coltext.

  APPEND wa_fieldcat TO t_fieldcat.
  CLEAR  wa_fieldcat.
ENDFORM.                               " FILL_FIELDCAT

*&---------------------------------------------------------------------*
*& Module STATUS_0500 OUTPUT
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
MODULE STATUS_0500 OUTPUT.
 SET PF-STATUS 'STATUS'.
 SET TITLEBAR 'TITLE'.
ENDMODULE.

*&---------------------------------------------------------------------*
*& Module CREATE_OBJECTS OUTPUT
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
MODULE CREATE_OBJECTS OUTPUT.
* Create a Docking container and dock the control at right side of screen
  CHECK r_dock_container IS  INITIAL.
  CREATE OBJECT r_dock_container
    EXPORTING
      side                        = cl_gui_docking_container=>DOCK_AT_BOTTOM
      extension                   = 270
      caption                     = 'Liste des articles'
    EXCEPTIONS
      cntl_error                  = 1
      cntl_system_error           = 2
      create_error                = 3
      lifetime_error              = 4
      lifetime_dynpro_dynpro_link = 5
      OTHERS                      = 6.
  IF sy-subrc <> 0.
MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
          WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  ENDIF.                               "  IF sy-subrc <> 0.

* To Create the Grid Instance
  CREATE OBJECT r_grid
    EXPORTING
      i_parent          = r_dock_container
    EXCEPTIONS
      error_cntl_create = 1
      error_cntl_init   = 2
      error_cntl_link   = 3
      error_dp_create   = 4
      OTHERS            = 5.
  IF sy-subrc <> 0.
 MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
             WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  ENDIF.                               " IF sy-subrc <> 0.

* Formatted Output Table is Sent to Control
  CALL METHOD r_grid->set_table_for_first_display
    CHANGING
      it_outtab                      =  t_mara
      it_fieldcatalog                =  t_fieldcat
*      it_sort                       =
*      it_filter                     =
    EXCEPTIONS
      invalid_parameter_combination = 1
      program_error                 = 2
      too_many_lines                = 3
      OTHERS                        = 4
          .
  IF sy-subrc <> 0.
   MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
              WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  ENDIF.                               " IF sy-subrc <> 0.
ENDMODULE.

*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0500  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE USER_COMMAND_0500 INPUT.

   CASE ok_code.
    WHEN 'EXECUTE'.
      SELECT matnr                     " material no.
             ersda                     " date of creation
             mtart                     " material type
             bismt                     " old material no.
             matkl                     " material group
             meins                     " base unit of measure
             brgew                     " gross weight
             ntgew                     " net weight
             gewei                     " weight unit
        FROM mara
        INTO TABLE t_mara
        WHERE ersda = MARA-ERSDA.
      IF sy-subrc <> 0.
      ENDIF.                           " IF sy-subrc EQ 0.
      CALL METHOD r_grid->refresh_table_display.
    WHEN 'BACK' OR 'CANCEL' OR 'EXIT'.
      LEAVE TO SCREEN 0.
  ENDCASE.                             " CASE ok_code.

ENDMODULE.