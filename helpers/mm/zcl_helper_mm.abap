class zcl_cev_helper_mm definition
  public
  final
  create public .

public section.
    interfaces zif_cev_hlpr_mm.
    aliases:
    get_article_land for zif_cev_hlpr_mm~get_article_origin_land.
protected section.
private section.

endclass.



class zcl_cev_helper_mm implementation.
  method get_article_land.

      data land_abr type land1.

      select single herkl
      from marc
      into @land_abr
      where matnr = @article
      and   werks = @division.

     full_land_name =
     zcl_cev_hlpr_common=>get_country_name(
         language          = 'FR'
         country_code      = land_abr
     ).

  endmethod.

endclass.