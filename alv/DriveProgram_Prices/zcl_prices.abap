*&---------------------------------------------------------------------*
*& DEFINITION : Prices for Brandt for Articles
*&---------------------------------------------------------------------*
class zcl_prices definition public final.


public section.

      types : begin of ty_prixbs,
                matnr type  mara-matnr,
                kbetr type  konp-kbetr,
              end of ty_prixbs.

      types : tty_prixbs  type standard table of ty_prixbs with default key,
              tty_matnr   type range of matnr.

      methods :
                constructor
                  importing
                      !it_articles type tty_matnr,
                get_articles
                    returning
                      value(rt_articles) type tty_matnr,
                get_prices
                  importing
                      it_articles type tty_matnr
                  returning
                      value(rt_result) type tty_prixbs,
                display_alv
                  importing
                      it_alvresults type tty_prixbs.

protected section.
    methods set_alv_header.
    methods set_alv_footer.
    methods set_toolbar.


private section.
        data: gt_articles       type tty_matnr,
              o_salv_display    type ref to cl_salv_table.

endclass.



class zcl_prices implementation.


  method constructor.
       me->gt_articles = it_articles.
  endmethod.


  method display_alv. "Display Result

      data: lt_alvresult type tty_prixbs,
            lr_columns type ref to cl_salv_columns_table,
            lr_column  type ref to cl_salv_column_table.

      " Passing the importing table to a local table (using the IT is not allowed directly)
      lt_alvresult = it_alvresults.


      try.
      call method cl_salv_table=>factory
      importing
      r_salv_table = me->o_salv_display
      changing
      t_table = lt_alvresult.

      " Get all columns data
      lr_columns = o_salv_display->get_columns( ).

      lr_columns->set_optimize( abap_true ).

          try.
              " Set column name for article
              lr_column ?= lr_columns->get_column( 'MATNR' ).
              lr_column->set_short_text( 'CodeArt' ).
              lr_column->set_medium_text( 'Code Article' ).
              lr_column->set_long_text( 'Code Article' ).

              " Set column name for the price
              lr_column ?= lr_columns->get_column( 'KBETR' ).
              lr_column->set_short_text( 'Prix' ).
              lr_column->set_medium_text( 'Prix Article' ).
              lr_column->set_long_text( 'Prix Article' ).

            catch cx_salv_not_found.
          endtry.


        me->set_alv_header( ).

        me->set_alv_footer( ).


        "Display the tool bar
        me->set_toolbar( ).


      " Display the result
      me->o_salv_display->display( ).

      catch cx_salv_msg .
      endtry.


  endmethod.


  method get_articles.
        rt_articles = me->gt_articles.
  endmethod.


  method set_toolbar.
      DATA functions TYPE REF TO cl_salv_functions_list.

      functions =  me->o_salv_display->get_functions( ).
      functions->set_all( abap_true ).
  endmethod.



   method get_prices.  "Get articles prices

    data :  lv_today     type sy-datum,
            wa_prixbs    type ty_prixbs,
            lx_exc       type ref to cx_root,
            lv_text      type string.

        lv_today = sy-datum.

        try.
            select mara~matnr, konp~kbetr
            from mara
            left outer join a004 on a004~matnr = mara~matnr
            left outer join konp on konp~knumh = a004~knumh
            into corresponding fields of table @rt_result
            where konp~kschl = 'ZS00'
            and   a004~vtweg = '20'
            and   a004~datbi >= @lv_today
            and   mara~matnr in @it_articles
            .
          catch cx_ecstat_query into lx_exc.
            lv_text = lx_exc->get_text( ).
            message lv_text type 'W'.
        endtry.

  endmethod.




method set_alv_header.

    data: column type i,
          row    type i.

    data: column_char type c,
          row_char    type c.

    data: text          type string,
          tooltip       type string,
          report_info   type string,
          current_user  type string.

    data icon type icon_d.

    data: o_header_element type ref to cl_salv_form_layout_grid,
          o_grid        type ref to cl_salv_form_layout_grid,

          o_header_info type ref to cl_salv_form_header_info,
          o_action_info type ref to cl_salv_form_action_info,

          o_text type ref to cl_salv_form_text,

          t_data_grid type standard table of
                           ref to cl_salv_form_layout_data_grid,
          o_data_grid type ref to cl_salv_form_layout_data_grid,

          o_icon type ref to cl_salv_form_icon.

*   Create an instance of Header objects
    create object o_header_element
      exporting
        columns = 2.

*   Create a Text with layout HEADER INFORMATION
    o_header_info = o_header_element->create_header_information(
      row = 1
      column = 1
      text     = 'Prix Brandt Store des produits'
      tooltip  = 'Tooltip of Header Info' ).

*   Create a Text with layout ACTION INFORMATION
    current_user = cl_abap_syst=>get_user_name( ).
    concatenate 'Lancé par'(ust) current_user  into report_info separated by space.
    o_action_info = o_header_element->create_action_information(
      row = 2
      column = 1
      text     = report_info
      tooltip  = 'Tooltip of Action Info' ).                "#EC NOTEXT


*   Set Header to Top of ALV
    o_salv_display->set_top_of_list( o_header_element ).

  endmethod.                    "set_alv_header





  method set_alv_footer.

    data: column type i,
          row    type i.

    data: column_char type c,
          row_char    type c.

    data: text    type string,
          tooltip type string.

    data icon type icon_d.

    data: o_footer_element type ref to cl_salv_form_layout_grid,
          o_grid           type ref to cl_salv_form_layout_grid,

          o_header_info type ref to cl_salv_form_header_info,
          o_action_info type ref to cl_salv_form_action_info,

          o_text type ref to cl_salv_form_text,

          t_data_grid type standard table of
                           ref to cl_salv_form_layout_data_grid,
          o_data_grid type ref to cl_salv_form_layout_data_grid,

          o_icon type ref to cl_salv_form_icon.

*   Create an instance of Footer objects
    create object o_footer_element
      exporting
        columns = 2.


*   Create a Grid with 2 rows and 3 columns
    o_grid = o_footer_element->create_grid( row = 2 column = 2 ).

    do 2 times.
      add 1 to row.

      do 1 times.
        add 2 to column.  "Column 1 is reserved to the icon

        move column to column_char.
        move row    to row_char.


       case row.
         when 1.
            concatenate 'Société :' 'Brandt Algérie'
                                      into text separated by space.
          move '@0N@' to icon. "Chart icon
          tooltip = 'OK Message'.


         when 2.
            concatenate 'Canal distribution :' '20'
                                      into text separated by space.
             move '@7Q@' to icon. "Truck icon
             tooltip = 'OK Message'.

       endcase.

*       Insert a text into on the <column> of <row> line
        o_text = o_grid->create_text(
            row     = row
            column  = column
            text    = text
            tooltip = tooltip ).

*       Get the layout of grid
        o_data_grid ?= o_text->get_layout_data( ).

        append o_data_grid to t_data_grid.
        clear o_text.

      enddo.


*     Set Icon on Grid
      create object o_icon
        exporting
          icon    = icon
          tooltip = tooltip.

*     Create an instance of Icon Objects
      call method o_grid->set_element
        exporting
          row       = row
          column    = 1
          r_element = o_icon.



      clear column.
    enddo.

*   Set Header to Top of ALV
    o_salv_display->set_end_of_list( o_footer_element ).

  endmethod.                    "set_alv_footer


  endclass.